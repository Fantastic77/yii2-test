<?php

use yii\db\Migration;

/**
 * Class m180531_124830_generate_test_numbers
 */
class m180531_124830_generate_test_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $n = 10000;
        for ($i = 1; $i <= $n; $i++){
            $this->insert('numbers', [
                    'value' => $i
                ]
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('numbers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180531_124830_generate_test_numbers cannot be reverted.\n";

        return false;
    }
    */
}
