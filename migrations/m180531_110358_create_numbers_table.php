<?php

use yii\db\Migration;

/**
 * Handles the creation of table `numbers`.
 */
class m180531_110358_create_numbers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('numbers', [
            'id' => $this->primaryKey(),
            'value' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('numbers');
    }
}
