<form action="edit" method="post">
    <table border="1">
        <?php for($i = 0; $i < $rows; $i++): ?>
            <tr>
                <?php for($j = 0; $j < $columns; $j++): ?>
                    <td>
                        <input type="number"
                               id="<?=$numbers[$i * $columns + $j]['id']?>"
                               name="<?=$numbers[$i * $columns + $j]['id']?>"
                               value="<?=$numbers[$i * $columns + $j]['value']?>" >
                    </td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>
    <input type="submit" value="Update">
</form>